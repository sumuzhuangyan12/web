###### 玩转金刚梯>金刚字典>
### Windows 操作系统

- 所谓<strong> Windows </strong>是[ 操作系统 ](/LadderFree/kkDictionary/OS.md)大家族中的一种
- <strong> Windows </strong> 是仅可安装在个人电脑(即PC)上的[ 操作系统 ](/LadderFree/kkDictionary/OS.md)
- <strong> Windows </strong> 是美国微软公司的产品

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)



