###### 玩转金刚梯>金刚字典>


### 金刚主号、金刚副号的用途
- [ 金刚主号 ](/LadderFree/kkDictionary/KKIDMain.md)的用途
  - 用于连通后翻墙出来访问被墙的
    - [ google ](https://google.com)搜索新闻
    - [ YouTube ](https://youtube.com)播放视频等
- [ 金刚副号 ](/LadderFree/kkDictionary/KKIDAuxiliary.md)的用途
  - 当[ 金刚主号 ](/LadderFree/kkDictionary/KKIDMain.md)
    - [ 流量提前耗尽 ](/LadderFree/kkDictionary/KKDataTrafficExhaustedEarly.md)或
    - [ 流量过期 ](/LadderFree/kkDictionary/KKDataTrafficExpired.md)时
  - 可立即连通[ 金刚副号 ](/LadderFree/kkDictionary/KKIDAuxiliary.md)[ 翻墙 ](/LadderFree/kkDictionary/OverTheWall.md)出来
    - 登录永远被墙的[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)
    - 为[ 金刚主号 ](/LadderFree/kkDictionary/KKIDMain.md)购买[ 流量 ](/LadderFree/kkDictionary/KKDataTraffic.md)
    - 而不必等到次日、利用[ 金刚主号 ](/LadderFree/kkDictionary/KKIDMain.md)次日的[ 免费流量 ](/kkdatatrafficfree.md)连通[ 金刚主号 ](/LadderFree/kkDictionary/KKIDMain.md)为[ 金刚主号 ](/LadderFree/kkDictionary/KKIDMain.md)购买[ 流量 ](/LadderFree/kkDictionary/KKDataTraffic.md)

- [ 金刚 ](/LadderFree/kkDictionary/Atozitpro.md)<strong> 强列建议 </strong >您：
  - 平时应至少持有两个[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)
  - 或[ 一主 ](/LadderFree/kkDictionary/KKIDMain.md)、[ 一副 ](/LadderFree/kkDictionary/KKIDAuxiliary.md)
  - 或两个[ 金刚主号 ](/LadderFree/kkDictionary/KKIDMain.md)
- 配置时应[ 注意事项 ](/LadderFree/kkDictionary/ConsiderationsWhileConfigureKKID.md)


#### 返回到
- [玩转金刚梯](/LadderFree/main.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

