###### 玩转金刚梯>金刚字典>

### 积分有效期

- [ 积分 ](/LadderFree/kkDictionary/KKPoints.md)，即金刚积分，余额有效期与[ 金刚公司 ](/LadderFree/kkDictionary/Atozitpro.md)寿命相同
- 只要[ 金刚公司 ](/LadderFree/kkDictionary/Atozitpro.md)存续，[ 金刚积分 ](/LadderFree/kkDictionary/KKPoints.md)就在有效期内



#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)



